Feature: Mission

  Scenario: Verify Who We are Mission and What We Do
    Given open itautomationno url
    Then it should display itautomation home page
    When click about option in itautomation home page
    Then it should display about us page
    Then validate who we are is shown in about us
    Then validate our mission is shown in about us
    Then validate what we do is shown in about us
