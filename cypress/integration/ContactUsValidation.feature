Feature: Validate Contact Us mandatory fields

Scenario: Verify mandatory error displayed for blank values
    Given open itautomationno url
    Then it should display itautomation home page
    And click contact in home page
    Then it should display contact us page
    And click submit
    Then validate mandatory error displayed for name 
    Then validate mandatory error displayed for email
    Then validate mandatory error displayed for message