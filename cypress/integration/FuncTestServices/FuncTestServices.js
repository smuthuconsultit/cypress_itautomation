import { Given, Then, When, And, when } from "cypress-cucumber-preprocessor/steps";
import itAutomationPage from '../pages/itAutomationPage';

when('click read more in functional testing', () => {
    itAutomationPage.clkFuncTestingRMBtn();
});

Then('validate functional testing page is shown', () => {
    itAutomationPage.funcTestingPage();
});