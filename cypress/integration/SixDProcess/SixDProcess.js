import { Given, Then, When, And } from "cypress-cucumber-preprocessor/steps";
import itAutomationPage from '../pages/itAutomationPage';

Then('about us page should show discover option under 6-D process',()=>{
    itAutomationPage.discover();
});

Then('about us page should show define option under 6-D process',()=>{
    itAutomationPage.define();
});

Then('about us page should show design option under 6-D process',()=>{
    itAutomationPage.design();
});

Then('about us page should show develop option under 6-D process',()=>{
    itAutomationPage.develop();
});

Then('about us page should show deploy option under 6-D process',()=>{
    itAutomationPage.deploy();
});

Then('about us page should show deliver option under 6-D process',()=>{
    itAutomationPage.deliver();
});