import { Given, Then, When, And, when } from "cypress-cucumber-preprocessor/steps";
import itAutomationPage from '../pages/itAutomationPage';

when('click read more in api testing', () => {
    itAutomationPage.clkAPITestingRMBtn();
});

Then('validate api testing page is shown', () => {
    itAutomationPage.apiTestingPage();
});