Feature: Why Choose Us

  Scenario: Why Choose Us
    Given open itautomationno url
    Then it should display itautomation home page
    When click about option in itautomation home page
    Then it should display about us page
    Then the best quality work should show in why choose us
    Then the support after delivery should show in why choose us
    Then the result oriented approach should show in why choose us
    Then the highly qualified team should show in why choose us
    Then the best roi techniques should show in why choose us
    Then the experienced professionals should show in why choose us

