Feature: Submit Contact Us request

Scenario Outline: Verify user can submit contact us request
    Given open itautomationno url
    Then it should display itautomation home page
    And click contact in home page
    Then it should display contact us page
    And enter name "<custname>"
    And enter email "<custemail>"
    And enter message "<messagenote>"
    And click submit
    Then verify success message for request submission

    Examples:
    |   custname    |   custemail   |   messagenote |
    |   name        |   email       |   message     |