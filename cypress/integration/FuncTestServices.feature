Feature: Verify Functional Testing page

  Scenario: Verify user can navigate to Functional Testing page
    Given open itautomationno url
    Then it should display itautomation home page
    And click services in home page
    Then it should display services page
    When click read more in functional testing
    Then validate functional testing page is shown
