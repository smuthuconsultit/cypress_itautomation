Feature: Verify AI Testing page

  Scenario: Verify user can navigate to AI Testing page
    Given open itautomationno url
    Then it should display itautomation home page
    And click services in home page
    Then it should display services page
    When click read more in ai testing
    Then validate ai testing page is shown