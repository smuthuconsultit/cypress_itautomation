import { Given, Then, When, And } from "cypress-cucumber-preprocessor/steps";
import itAutomationPage from '../pages/itAutomationPage';

Then('validate who we are is shown in about us',()=>{
    itAutomationPage.whoAreWe();
});

Then('validate our mission is shown in about us',()=>{
    itAutomationPage.mission();
});

Then('validate what we do is shown in about us',()=>{
    itAutomationPage.whatWeDo();
});