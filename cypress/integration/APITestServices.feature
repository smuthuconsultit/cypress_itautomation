Feature: Verify API Testing page

  Scenario: Verify user can navigate to API Testing page
    Given open itautomationno url
    Then it should display itautomation home page
    And click services in home page
    Then it should display services page
    When click read more in api testing
    Then validate api testing page is shown