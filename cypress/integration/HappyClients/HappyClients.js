import { Given, Then, When, And } from "cypress-cucumber-preprocessor/steps";
import itAutomationPage from '../pages/itAutomationPage';

Then('about us page should display total projects completed',()=>{
    itAutomationPage.projectedCompleted();
});

Then('about us page should display happy clients',()=>{
    itAutomationPage.happyClients();
});

Then('about us page should display awards',()=>{
    itAutomationPage.awards();
});