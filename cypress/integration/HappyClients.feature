Feature: Happy Clients

  Scenario: Verify Happy Clients Total Projects and Awards
    Given open itautomationno url
    Then it should display itautomation home page
    When click about option in itautomation home page
    Then it should display about us page
    Then about us page should display total projects completed
    Then about us page should display happy clients
    Then about us page should display awards

