Feature: 6-D Process

  Scenario: Our 6-D Process
    Given open itautomationno url
    Then it should display itautomation home page
    When click about option in itautomation home page
    Then it should display about us page
    Then about us page should show discover option under 6-D process
    Then about us page should show define option under 6-D process
    Then about us page should show design option under 6-D process
    Then about us page should show develop option under 6-D process
    Then about us page should show deploy option under 6-D process
    Then about us page should show deliver option under 6-D process

