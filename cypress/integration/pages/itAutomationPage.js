//import { assert } from "sinon";
var chai = require('chai'); 
var expect = chai.expect;

const itAutomation_Home = '//a[text()=\'Home\']';
const contact = '//a[@href=\'https://itautomation.no/contact/\' and text()=\'Contact\']';
const contactUsPage = '//h1[text()=\'Contact Us\']';
const contactus_Name = '//input[@id=\'wpforms-6-field_0\']';
const contactus_Email = '//input[@id=\'wpforms-6-field_1\']'; 
const contactus_Message = '//textarea[@id=\'wpforms-6-field_2\']';
const contactus_nameValMsg = '//label[@for=\'wpforms-6-field_0\' and contains(text(),\'This field is required.\')]';
const contactus_emailValMsg = '//label[@for=\'wpforms-6-field_1\' and contains(text(),\'This field is required.\')]';
const contactus_messageValMsg = '//label[@for=\'wpforms-6-field_2\' and contains(text(),\'This field is required.\')]';
const contactus_SubmitBtn = '//button[@id=\'wpforms-submit-6\' and text()=\'Submit\']';
const cntactus_SuccessMsg = '//p[text()=\'Thanks for contacting us! We will be in touch with you shortly.\']';
const services = '//a[@href=\'https://itautomation.no/services/\']';
const servicesPage = '//h1[text()=\'Services\']';
const aiTesting_ReadMore = "//a[@href='https://itautomation.no/aitest/']/span/span";
const ai_Testing = '//h4[contains(text(),\'AI Testing\')]';
const ai_TestingRM = '//h3[text()=\'AI Testing\']';
const apiTesting_ReadMore = "//a[@href='https://itautomation.no/apitest/']/span/span";
const api_Testing = '//h4[contains(text(),\'API Testing\')]';
const api_TestingRM = '//h3[contains(text(),\'API Testing\')]';
const funcTesting_ReadMore = "//a[@href='https://itautomation.no/functest/']/span/span";
const functional_Testing = '//h4[contains(text(),\'Functional Testing\')]';
const functional_TestingRM = '//h3[contains(text(),\'Functional Testing\')]';
const about = '//a[text()=\'About\']';
const aboutUs = '//h1[text()=\'About Us\']';
const awards = '//span[@data-to-value=\'2\']';
const happyClients = '//span[@data-to-value=\'5\']';
const projectsCompleted = '//span[@data-to-value=\'17\']';
const whoAreWe = "//div[@data-id='7mowx5g']//h3";
const ourMission = "//div[@data-id='fwplb74']//h3[text()='Our Mission']";
const WhatWeDo = "//div[@data-id='ykcakpi']//h3";
const whoAreWeMsg = "//div[@data-id='9ung4kd']//p[contains(text(),'We are a dedicated and expert team with holistic experience and knowledge of software test automation.')]";
const ourMissionMsg = "//div[@data-id='pp0r1br']//p[contains(text(),'To make our customers happy by providing robust/quality automation')]";
const whatWeDoMsg = "//div[@data-id='wt82puy']//p[contains(text(),'We are working cohesively to make QA outsourcing and test automation services,')]";
const our6DProcess = "//h3[text()='Our 6-D process']";
const discover = "//h2[text()='Discover']";
const define = "//h2[text()='Define']";
const design = "//h2[text()='Design']";
const develop = "//h2[text()='Develop']";
const deploy = "//h2[text()='Deploy']";
const deliver = "//h2[text()='Deliver']";
const whyChooseUs = "//h3[text()='Why choose us?']";
const supportAfterDelivery = "//a[text()='Support after Delivery/Handover']";
const bestQualityWorkMsg = "//div[@id='elementor-tab-content-1891']/p[contains(text(),'Quality is our primary focus, and we ensure everytime on our delivery.')]";
const supportAfterDeliveryMsg = "//div[@id='elementor-tab-content-1892']/p[contains(text(),'After project delivery, we support our customer for 3 months')]";
const resultOrientedApproachMsg = "//div[@id='elementor-tab-content-1893']/p[contains(text(),'We work towards specific targets, set delivery deadlines and prioritize problem-solving')]";
const highlyQualifiedTeamMsg = "//div[@id='elementor-tab-content-1894']/p[contains(text(),'Our team is one of the greatest assets in delivering test automation solutions and services.')]";
const bestROITechniquesMsg = "//div[@id='elementor-tab-content-1895']/p[contains(text(),'Our ROI calculator clearly shows how effectively our stratergy works')]";
const experiencedProfessionalsMsg = "//div[@id='elementor-tab-content-1896']/p[contains(text(),'Our team of specialists is something that makes us extremely proud and self-confident.')]";
const resultOrientedApproach = "//a[text()='Result Oriented Approach']";
const highlyQualifiedTeam = "//a[text()='Highly Qualified Team']";
const bestROITechniques = "//a[text()='Best ROI Techniques']";
const experiencedProfessionals = "//a[text()='Experienced Professionals']";

class itAutomationPage {

    static openItAutomationUrl(){
        cy.visit(Cypress.env('itAutomationUrl'));
        //cy.visit(Cypress.env('url'));        
        cy.wait(2000);
    }

    static itAutomationHomePage(){
        cy.xpath(itAutomation_Home).should("be.visible");
    }

    static contactMenu(){
        cy.xpath(contact).click();
    }

    static checkContactUsPage(){
        cy.xpath(contactUsPage).should('be.visible');
    }

    static cntusName(reqname){
        cy.xpath(contactus_Name).type(Cypress.env(reqname));
    }

    static cntusEmail(reqemail){
        cy.xpath(contactus_Email).type(Cypress.env(reqemail));
    }

    static cntusMessage(reqmsg){
        cy.xpath(contactus_Message).type(Cypress.env(reqmsg));
    }

    static cntusSubmitBtn(){
        cy.xpath(contactus_SubmitBtn).click();
    }

    static cntusSubmissionSuccMsg(){
        cy.xpath(cntactus_SuccessMsg).should("be.visible");
    }

    static cntusnameValErr(){
        cy.xpath(contactus_nameValMsg).should("be.visible");
    }

    static cntusemailValErr(){
        cy.xpath(contactus_emailValMsg).should("be.visible");
    }

    static cntusmessageValErr(){
        cy.xpath(contactus_messageValMsg).should("be.visible");
    }

    static servicesMenu(){
        cy.xpath(services).click();
    }

    static servicesPage(){
        cy.xpath(servicesPage).should("be.visible");
    }

    static clkAITestingReadMoreBtn(){
        cy.xpath(aiTesting_ReadMore).should("be.visible").click();
    }

    static aiTestingPage(){
        cy.xpath(ai_Testing).should("be.visible");
        //cy.get('div').contains('AI Testingg').should('have.attr', 'class', 'elementor-heading-title elementor-size-default')
    }

    static clkAPITestingRMBtn(){
        cy.xpath(apiTesting_ReadMore).should("be.visible").click();
    }

    static apiTestingPage(){
        cy.xpath(api_Testing).should("be.visible");
    }

    static clkFuncTestingRMBtn(){
        cy.xpath(funcTesting_ReadMore).should("be.visible").click();
    }

    static funcTestingPage(){
        cy.xpath(functional_Testing).should("be.visible");
    }

    static clkAbout(){
        cy.xpath(about).should("be.visible").click();
    }

    static aboutUsPage(){
        cy.xpath(aboutUs).should("be.visible");
    }

    static projectedCompleted(){
        cy.get('footer').scrollIntoView({ duration: 2000 })
        cy.xpath(projectsCompleted).should("be.visible");
    }

    static awards(){
        //cy.xpath(awards).should("be.visible");
        //var x = cy.xpath(awards).text();
        //console.log("zx: "+x);
        //assert.equal("3","2","text not matched");
        //var l = cy.xpath(awards).getText();
        //console.log("text: ",cy.xpath(awards).getText());
        //cy.get('span').contains('2').should('eq', '2');
        cy.get('span').contains('2').should('be.visible').and('eq', '2');

    }

    static happyClients(){
        cy.xpath(happyClients).should("be.visible");
    }

    static whoAreWe(){
        cy.xpath(whoAreWe).should("be.visible");
        cy.xpath(whoAreWeMsg).should("be.visible");
    }

    static mission(){
        cy.xpath(ourMission).should("be.visible");
        cy.xpath(ourMissionMsg).should("be.visible");
    }

    static whatWeDo(){
        cy.xpath(WhatWeDo).should("be.visible");
        cy.xpath(whatWeDoMsg).should("be.visible");
    }

    static discover(){
        cy.scrollTo(0,700);
        cy.xpath(discover).should("be.visible");
    }

    static define(){
        cy.xpath(define).should("be.visible");
    }

    static design(){
        cy.xpath(design).should("be.visible");
    }

    static develop(){
        cy.xpath(develop).should("be.visible");
    }

    static deploy(){
        cy.xpath(deploy).should("be.visible");
    }

    static deliver(){
        cy.xpath(deliver).should("be.visible");
    }

    static bestQualityWork(){
        cy.scrollTo(0,700);
        cy.xpath(bestQualityWorkMsg).should('be.visible');
    }

    static supportAfterDelivery(){
        cy.xpath(supportAfterDelivery).click();
        cy.xpath(supportAfterDeliveryMsg).should('be.visible');
    }

    static resultOrientedApproach(){
        cy.xpath(resultOrientedApproach).click();
        cy.xpath(resultOrientedApproachMsg).should('be.visible');
    }

    static highlyQualifiedTeam(){
        cy.scrollTo(0,1200);
        cy.xpath(highlyQualifiedTeam).click();
        cy.xpath(highlyQualifiedTeamMsg).should('be.visible');
    }

    static bestROITechniques(){
        cy.xpath(bestROITechniques).click();
        cy.xpath(bestROITechniquesMsg).should('be.visible');
    }

    static experiencedProfessionals(){
        cy.xpath(experiencedProfessionals).click();
        cy.xpath(experiencedProfessionalsMsg).should('be.visible');
    }
    

}

export default itAutomationPage