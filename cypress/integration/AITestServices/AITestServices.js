import { Given, Then, And, when } from "cypress-cucumber-preprocessor/steps";
import itAutomationPage from '../pages/itAutomationPage';

    when('click read more in ai testing', () => {
        itAutomationPage.clkAITestingReadMoreBtn();
    });

    Then('validate ai testing page is shown', () => {
        itAutomationPage.aiTestingPage();
    });