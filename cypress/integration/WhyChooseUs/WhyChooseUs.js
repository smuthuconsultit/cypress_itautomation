import { Given, Then, When, And } from "cypress-cucumber-preprocessor/steps";
import itAutomationPage from '../pages/itAutomationPage';


Then('the best quality work should show in why choose us', () => {
    itAutomationPage.bestQualityWork();
});

Then('the support after delivery should show in why choose us', () => {
    itAutomationPage.supportAfterDelivery();
});

Then('the result oriented approach should show in why choose us', () => {
    itAutomationPage.resultOrientedApproach();
});

Then('the highly qualified team should show in why choose us', () => {
    itAutomationPage.highlyQualifiedTeam();
});

Then('the best roi techniques should show in why choose us', () => {
    itAutomationPage.bestROITechniques();
});

Then('the experienced professionals should show in why choose us', () => {
    itAutomationPage.experiencedProfessionals();
});