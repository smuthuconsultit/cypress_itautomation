import { Given, Then, When, And } from "cypress-cucumber-preprocessor/steps";
import itAutomationPage from '../pages/itAutomationPage';

    And('click submit', () => {
        itAutomationPage.cntusSubmitBtn();
    });

    Then('validate mandatory error displayed for name', () => {
        itAutomationPage.cntusnameValErr();
    });

    Then('validate mandatory error displayed for email', () => {
        itAutomationPage.cntusemailValErr();
    });

    Then('validate mandatory error displayed for message', () => {
        itAutomationPage.cntusmessageValErr();
    });