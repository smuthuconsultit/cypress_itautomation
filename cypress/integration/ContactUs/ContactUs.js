import { Given, Then, When, And } from "cypress-cucumber-preprocessor/steps";
import itAutomationPage from '../pages/itAutomationPage';

    And('enter name {string}', (reqname) => {
        itAutomationPage.cntusName(reqname);
    });

    And('enter email {string}', (reqemail) => {
        itAutomationPage.cntusEmail(reqemail);
    });

    And('enter message {string}', (reqmsg) => {
        itAutomationPage.cntusMessage(reqmsg);
    });

    And('click submit', () => {
        itAutomationPage.cntusSubmitBtn();
    });

    Then('verify success message for request submission', () => {
        itAutomationPage.cntusSubmissionSuccMsg();
    });