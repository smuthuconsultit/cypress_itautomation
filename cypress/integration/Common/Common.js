import { Given, Then, When, And } from "cypress-cucumber-preprocessor/steps";
import itAutomationPage from '../pages/itAutomationPage';

    Given('open itautomationno url', () => {
        itAutomationPage.openItAutomationUrl();
    });

    Then('it should display itautomation home page', () => {
        itAutomationPage.itAutomationHomePage();
    });

    And('click contact in home page', () => {
        itAutomationPage.contactMenu();
    });

    Then('it should display contact us page', () => {
        itAutomationPage.checkContactUsPage();
    });

    And('click services in home page', () => {
        itAutomationPage.servicesMenu();
    });

    Then('it should display services page', () => {
        itAutomationPage.servicesPage();
    });

    When('click about option in itautomation home page',()=>{
        itAutomationPage.clkAbout();
    });
    
    Then('it should display about us page',()=>{
        itAutomationPage.aboutUsPage();
    });