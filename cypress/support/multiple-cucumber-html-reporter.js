const report = require('multiple-cucumber-html-reporter');
 
report.generate({
    jsonDir: 'cypress/cucumber-json',
    reportPath: 'cypress/multipleCucumberReports',
    metadata:{
        browser: {
            name: 'chrome',
            version: '86'
        },
        device: 'Local test machine',
        platform: {
            name: 'windows',
            version: '10'
        }
    },
    customData: {
        title: 'Run info',
        data: [
            {label: 'Project', value: 'IT Automation'},
            {label: 'Release', value: '1.0'},
            {label: 'Cycle', value: 'C1'},
            {label: 'Execution Start Time', value: 'Nov 17th 2020'},
            {label: 'Execution End Time', value: 'Nov 17th 2020'}
        ]
    }
});